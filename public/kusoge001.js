import React, {Component} from 'react';
import {Container} from 'reactstrap';
import 'react-canvas';
import {Layer, Rect, Stage, Group, Star} from 'react-konva';

var rect = (props) => {
    const {ctx, x, y, width, height} = props;
    ctx.fillRect(x, y, width, height);
};

class Playable_Character {
    constructor() {
        this.state = {
            row: 0,
        }
    }

    reset() {
        this.setState({row: 0});
    }
}

class Item {
    constructor() {
        this.state = {
            rank: 0,
            row: 0,
            col: 0,
        }
    }

    shiftLeft() {
        this.setState({col: this.state.col-1})
    }
}

function GetChance() {
    return Math.random();
}

export default class UsiZip extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: window.innerWidth,
            height: window.innerHeight,
            item_list: [],
            rowN: 5,
            colN: 15,
            score_current: 0,
            score_goal: 5000,
            pc: Playable_Character(),
        };
        this.move = this.move.bind(this);
    }

    componentWillMount() {
        // setup the playable character
        this.state.pc.reset();
    }

    addItem(rank, row) {
        let item = Item();
        item.row = row;
        item.rank = rank;
        item.col = this.state.colN;

        let update = this.state.item_list;
        update = append(update, item);
        this.setState({item_list: update});
    }

    addRandItems() {
        let update = this.state.item_list;
        for(let i = 0; i < this.state.rowN; i+=1) {
            if(GetChance() > 0.5) {
                this.addItem(100 * GetChance() * 100, i);
            }
        }
    }

    move() {
        let update = this.state.item_list;
        // move all by one
        item_list.map( (item) => {item.shiftLeft()} );

        // only the front items + colN is going to go away at most.
        // only the second front items + colN is going to encounter pc.
        for(let frontCol = 0; frontCol < this.state.colN*2 && frontCol < update.length; frontCol+=1) {
            if(update[frontCol].col < 0) {
                this.setState({item_list: remove(update, frontCol)})
                frontCol-=1;
            } else if(update[frontCol].col == 0 && update[frontCol].row == this.state.pc.row) {
                // Gameover
                alert("Game Over");
                return;
            }
        }

        this.setState({item_list: update});
    }

    componentDidMount() {
        this.updateCanvas();
    }
    componentDidUpdate() {
        this.updateCanvas();
    }
    updateCanvas() {
        const ctx = this.refs.canvas.getContext('2d');
        ctx.clearRect(0,0, 300, 300);
        // draw children “components”
        rect({ctx, x: 10, y: 10, width: 50, height: 50});
        rect({ctx, x: 110, y: 110, width: 50, height: 50});
    }

    render() {
        // after some interval of ticks
        this.move();

        return(
            <div id="usi.zip">
                {this.state.score_current}
                <Container onChange={}>
                    <Row className="col-xs-12 main-screen">
                        <canvas ref="canvas" width={640} height={425} />
                    </Row>
                    <Row className="col-xs-12 btns">
                        <ButtonGroup className="ctl-btn">
                            <Button className="col-xs-4 pause" onClick={}/>
                            <Button className="col-xs-4 start" onClick={}/>
                            <Button className="col-xs-4 reset" onClick={}/>
                        </ButtonGroup>
                    </Row>
                </Container>
            </div>
        );
    }
}