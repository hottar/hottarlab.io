
import React, {Component} from 'react';
import {Rect} from 'react-konva';

export default class Playable_Character extends Component {
    constructor(props) {
        super(props);
        this.state = {
            x: 0,
            y: 0,
            vx: 0,
            vy: 0,
            scale: 100,
        }
    }

    componentWillMount() {
        this.setState({x: this.props.col * this.props.unit, y: this.props.row * this.props.unit, scale: this.props.unit});
    }

    update() {
        let nextX = this.state.x - this.props.unit;
        this.setState({x: nextX});

        // over boundary
        if(nextX < 0) {
            delete this;
        }
    }

    render() {
        return(
            <Rect x={this.state.x} y={this.state.y} width={this.props.unit} height={this.props.unit}
                fill={'red'} ref={'rect'} />
        )
    }
}