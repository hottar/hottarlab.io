
import React, {Component} from 'react';
import {Rect} from 'react-konva';

export default class Playable_Character extends Component {
    constructor(props) {
        super(props);
        this.state = {
            x: 0,
            y: 0,
            vx: 0,
            vy: 0,
            scale: 100,
        }
    }

    componentWillMount() {
        this.setState({x: this.props.x, y: this.props.y, scale: this.props.scale});
    }

    componentShouldUpdate(nextProps) {
        return nextProps.inputKeyDirection !== this.props.inputKeyDirection;
    }

    componentWillUpdate() {
        if( componentShouldUpdate() ) {
            this.setState({y: this.state.y + this.props.inputKeyDirection});
        }
    }

    render() {
        return(
            <Rect x={this.state.x} y={this.state.y} width={this.state.scale} height={this.state.scale}
                fill={'green'} draggable={true} ref={'rect'} />
        )
    }
}